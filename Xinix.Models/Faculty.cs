using System;
using System.Collections.Generic;

namespace Xinix.Models
{
    public class Faculty
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public List<Student> Students { get; set; }
        public Teacher Curator { get; set; }
    }
}