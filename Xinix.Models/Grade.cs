using System.Guid;

namespace Xinix.Models
{
    public class Grade
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Subject Subject { get; set; }
        public int Grade { get; set; }
        public Teacher Teacher { get; set; }
        public DateTime Date { get; set; }
    }
}