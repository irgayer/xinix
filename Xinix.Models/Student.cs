﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xinix.Models
{
    public class Student : Person
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Firstname { get; set; }
        public string Secondname { get; set; }
        public string Patronymic { get; set; }
        public List<Grade> Grades { get; set; }
    }
}
