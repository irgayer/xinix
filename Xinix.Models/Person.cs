﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xinix.Models
{
    public abstract class Person 
    {
        Guid Id { get; set; }
        string Firstname { get; set; }
        string Secondname { get; set; }
        string Patronymic { get; set; }
    }
}
