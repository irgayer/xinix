Xinix
======
**xinix** - приложение, упрощающее составление расписания для учебных заведений.

![gifochaka](https://media1.tenor.com/images/f8539f656d2ed90be7cd3bbe95d263d2/tenor.gif)

|Проект             |Платформа            |Шаблон       |Описание                       |
|:-----------------:|---------------------|-------------|-------------------------------|
|Xinix.**Desktop**  | .NET *Core 3.1*     | WPF         | Приложение для Windows        |
|Xinix.**Web**      | .NET *Core 3.1*     | MVC         | Сайт                          |
|Xinix.**Services** | .NET *Core 3.1* | classlib    | Классы выполняющие дополнительные действия              |
|Xinix.**Logic**    | .NET *Core 3.1* | classlib    | Классы бизнес-логики              |
|Xinix.**Models**   | .NET *Standard 2.0* | classlib    | Классы предметной области     |

TODO:
----
- [ ] Обдумать нормальную архитектуру
- [ ] Алгоритм
- [ ] Написать бэкенд
- [ ] Не запутаться в своём же проекте
- [ ] Сверстать невырвиглазный дизайн
- [ ] front + back = magic

![gifochaka](https://media1.tenor.com/images/59371e16bf2c92a158a0bf84e1e70bb6/tenor.gif)

**будут правки**